#!/usr/bin/env python3

# Process CSV exports of MetroRegion and ZipCode queries into:
# a) gperf input to create hash tables
# b) static array of IATA metro codes

import csv

METRO_DATA = "metros.csv"
METRO_ARRAY_OUTPUT = "../src/metros.h"
METRO_GPERF_OUTPUT = "metro_gperf.txt"

def generate_gperf_input_metro_loc():
    with open(METRO_GPERF_OUTPUT, 'w') as fo:
        fo.write("%define hash-function-name hash_metro\n")
        fo.write("%define lookup-function-name in_word_set_metro\n")
        fo.write("%define slot-name iata\n")
        fo.write("%readonly-tables\n")
        fo.write("struct metro_location { char *iata; float latitude; float longitude; unsigned int local_range; };\n")
        fo.write("%%\n")
        with open(METRO_DATA, 'r') as fi:
            csvr = csv.reader(fi)
            for row in csvr:
                fo.write("{}, {}, {}, {}\n".format(row[0], row[1], row[2], row[3]))
        fo.write("%%\n")

def generate_static_metro_array():
    lines = list()
    with open(METRO_DATA, 'r') as fi:
        csvr = csv.reader(fi)
        for row in csvr:
            lines.append("\t\"{}\",\n".format(row[0]))
    lines[-1] = lines[-1].replace(',','')
    with open(METRO_ARRAY_OUTPUT, 'w') as fo:
        fo.write("/* Static array of metro region IATA codes */\n")
        fo.write("const char *metros[] = {\n")
        for l in lines:
            fo.write(l)
        fo.write("};")
            
if __name__ == '__main__':
    generate_gperf_input_metro_loc()
    generate_static_metro_array()
            
    


