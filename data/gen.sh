#!/bin/bash

echo "Generating headers and gperf input"

./gen.py

echo "Generating metro location mapping"

gperf -t -S 1 --output-file=../src/metro_loc.h ./metro_gperf.txt

echo "Done"
