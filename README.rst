============
vmod_geode
============

-----------------------------------------------
Geolocation and Device Detection Varnish Module
-----------------------------------------------

:Author: Benjamen Keroack
:Date: 2014-08-26
:Version: 1.0
:Manual section: 3

SYNOPSIS
========

::

            import geode;

DESCRIPTION
===========

Varnish module which supports device and location detection (for adding to headers, etc) via 51Degrees and GeoIP, respectively.


FUNCTIONS
=========

.. NOTE::
   All functions returning strings will either return a valid result or--in the case of an error or data not found--will return an error message in parentheses.
   Therefore testing the first character of the response for '(' is a valid test for success.

ismobile
--------

Prototype
        ::

                ismobile(STRING user_agent)
Return value
	STRING
Description
	Returns "True" if user agent is detected as being from a mobile device, "False" if not or "(none)" if error or user agent not found.
Example
        ::

                set resp.http.Is-Mobile = geode.ismobile(req.http.User-Agent);


mobile_screenheight
-------------------

Prototype
        ::

                mobile_screenheight(STRING user_agent)
Return value
	STRING
Description
	Returns a string containing the vertical resolution in pixels of client device, "Unknown" if not a mobile device or "(none)" if error or user agent not found.
Example
        ::

                set resp.http.Mobile-Screen-Height = geode.mobile_screenheight(req.http.User-Agent);


mobile_screenwidth
------------------

Prototype
        ::

                mobile_screenwidth(STRING user_agent)
Return value
	STRING
Description
	Returns a string containing the horizontal resolution in pixels of client device, "Unknown" if not a mobile device or "(none)" if error or user agent not found.
Example
        ::

                set resp.http.Mobile-Screen-Width = geode.mobile_screenwidth(req.http.User-Agent);


zipcode
-------

Prototype
        ::

                zipcode(STRING ip)
Return value
	STRING
Description
    Returns a string containing the detected postal code for the given IP, or one of the following error messages:
    
    * "(IP not found)" - IP not found in GeoIP database
    * "(postal code not found)" - IP found but postal code information is not available
    * "(bad IP string!)" - malformed IP string
    * "(db not initialized!)" - internal error
    
    .. NOTE::
       IP must be a string. Convert client IP to string by adding to empty string.
Example
        ::

                set resp.http.Zipcode = geode.zipcode("" + client.ip);
                
latitude
--------

Prototype
        ::

                latitude(STRING ip)
Return value
	STRING
Description
    Returns a string containing the detected latitude for the given IP, or one of the following error messages:
    
    * "(IP not found)" - IP not found in GeoIP database
    * "(latitude not found)" - IP found but latitude information is not available
    * "(bad IP string!)" - malformed IP string
    * "(db not initialized!)" - internal error
            
    .. NOTE::
       IP must be a string. Convert client IP to string by adding to empty string.
Example
        ::

                set resp.http.Latitude = geode.latitude("" + client.ip);
                
longitude
---------

Prototype
        ::

                longitude(STRING ip)
Return value
	STRING
Description
    Returns a string containing the detected longitude for the given IP, or one of the following error messages:
    
    * "(IP not found)" - IP not found in GeoIP database
    * "(longitude not found)" - IP found but longitude information is not available
    * "(bad IP string!)" - malformed IP string
    * "(db not initialized!)" - internal error
    
    .. NOTE::
       IP must be a string. Convert client IP to string by adding to empty string.
Example
        ::

                set resp.http.Longitude = geode.longitude("" + client.ip);
                
metrocode
---------

Prototype
        ::

                metrocode(STRING ip)
Return value
	STRING
Description
    Returns a string containing the geographically closest IATA metro code for the given IP, or one of the following error messages:
    
    * "(IP not found)" - IP not found in GeoIP database
    * "(coordinates not found)" - IP found but coordinate information is not available
    * "(bad IP string!)" - malformed IP string
    * "(db not initialized!)" - internal error
    * "(error)" - internal error calculating distance
    
    .. NOTE::
       IP must be a string. Convert client IP to string by adding to empty string.
       
    .. ATTENTION::
       Metro code data is in /data/ subdirectory. By default it only includes continental US metro codes.
Example
        ::

                set resp.http.Metro = geode.metrocode("" + client.ip);

INSTALLATION
============

Steps:
------
    
    1. Install prerequisites and build module per below.
    2. Download 51Degrees lite database (http://sourceforge.net/projects/fiftyone-c/). 
       Within the C API zip archive, copy data/51Degrees-Lite.dat (not trie) to /etc/varnish/51Degrees-Lite.dat
    3. Download Geolite City database (http://dev.maxmind.com/geoip/legacy/geolite/). 
       Decompress and copy GeoLiteCity.dat to /usr/share/GeoIP/GeoLiteCity.dat

Usage::

 # prereqs
 sudo apt-get install varnish libvarnishapi-dev automake autoconf libtool pkg-config libpcre3 libpcre3-dev python-docutils libgeoip-dev libedit-dev
 # get varnish source and build it
 apt-get source varnish
 cd varnish-3.0.x
 ./configure
 make
 # build and install geode
 cd libvmod_geode
 ./autogen.sh
 ./configure VARNISHSRC=../varnish-3.0.x [VMODDIR=DIR]
 make
 sudo make install

`VARNISHSRC` is the directory of the Varnish source tree for which to
compile your vmod. Both the `VARNISHSRC` and `VARNISHSRC/include`
will be added to the include search paths for your module.

Optionally you can also set the vmod install directory by adding
`VMODDIR=DIR` (defaults to the pkg-config discovered directory from your
Varnish installation).

Make targets:

* make - builds the vmod
* make install - installs your vmod in `VMODDIR`
* make check - runs the unit tests in ``src/tests/*.vtc``

COPYRIGHT
=========

See LICENSE for details.

* Copyright (c) 2014 ScoreBig, Inc.
