/* C code produced by gperf version 3.0.3 */
/* Command-line: /Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/gperf -t -S 1 --output-file=../src/metro_loc.h ./metro_gperf.txt  */
/* Computed positions: -k'1-3' */

#if !((' ' == 32) && ('!' == 33) && ('"' == 34) && ('#' == 35) \
      && ('%' == 37) && ('&' == 38) && ('\'' == 39) && ('(' == 40) \
      && (')' == 41) && ('*' == 42) && ('+' == 43) && (',' == 44) \
      && ('-' == 45) && ('.' == 46) && ('/' == 47) && ('0' == 48) \
      && ('1' == 49) && ('2' == 50) && ('3' == 51) && ('4' == 52) \
      && ('5' == 53) && ('6' == 54) && ('7' == 55) && ('8' == 56) \
      && ('9' == 57) && (':' == 58) && (';' == 59) && ('<' == 60) \
      && ('=' == 61) && ('>' == 62) && ('?' == 63) && ('A' == 65) \
      && ('B' == 66) && ('C' == 67) && ('D' == 68) && ('E' == 69) \
      && ('F' == 70) && ('G' == 71) && ('H' == 72) && ('I' == 73) \
      && ('J' == 74) && ('K' == 75) && ('L' == 76) && ('M' == 77) \
      && ('N' == 78) && ('O' == 79) && ('P' == 80) && ('Q' == 81) \
      && ('R' == 82) && ('S' == 83) && ('T' == 84) && ('U' == 85) \
      && ('V' == 86) && ('W' == 87) && ('X' == 88) && ('Y' == 89) \
      && ('Z' == 90) && ('[' == 91) && ('\\' == 92) && (']' == 93) \
      && ('^' == 94) && ('_' == 95) && ('a' == 97) && ('b' == 98) \
      && ('c' == 99) && ('d' == 100) && ('e' == 101) && ('f' == 102) \
      && ('g' == 103) && ('h' == 104) && ('i' == 105) && ('j' == 106) \
      && ('k' == 107) && ('l' == 108) && ('m' == 109) && ('n' == 110) \
      && ('o' == 111) && ('p' == 112) && ('q' == 113) && ('r' == 114) \
      && ('s' == 115) && ('t' == 116) && ('u' == 117) && ('v' == 118) \
      && ('w' == 119) && ('x' == 120) && ('y' == 121) && ('z' == 122) \
      && ('{' == 123) && ('|' == 124) && ('}' == 125) && ('~' == 126))
/* The character set is not based on ISO-646.  */
error "gperf generated tables don't work with this execution character set. Please report a bug to <bug-gnu-gperf@gnu.org>."
#endif

#line 5 "./metro_gperf.txt"
struct metro_location { char *iata; float latitude; float longitude; unsigned int local_range; };

#define TOTAL_KEYWORDS 54
#define MIN_WORD_LENGTH 3
#define MAX_WORD_LENGTH 3
#define MIN_HASH_VALUE 9
#define MAX_HASH_VALUE 154
/* maximum key range = 146, duplicates = 0 */

#ifdef __GNUC__
__inline
#else
#ifdef __cplusplus
inline
#endif
#endif
static unsigned int
hash_metro (str, len)
     register const char *str;
     register unsigned int len;
{
  static const unsigned char asso_values[] =
    {
      155, 155, 155, 155, 155, 155, 155, 155, 155, 155,
      155, 155, 155, 155, 155, 155, 155, 155, 155, 155,
      155, 155, 155, 155, 155, 155, 155, 155, 155, 155,
      155, 155, 155, 155, 155, 155, 155, 155, 155, 155,
      155, 155, 155, 155, 155, 155, 155, 155, 155, 155,
      155, 155, 155, 155, 155, 155, 155, 155, 155, 155,
      155, 155, 155, 155, 155,   5,  45,   0,  40,  11,
       55,  25,  60,  35,  55,  30,   0,   5,   1,  26,
       35,  51,  50,  10,  20,  51,  50,  55,  36,  56,
      155, 155, 155, 155, 155, 155, 155, 155, 155, 155,
      155, 155, 155, 155, 155, 155, 155, 155, 155, 155,
      155, 155, 155, 155, 155, 155, 155, 155, 155, 155,
      155, 155, 155, 155, 155, 155, 155, 155, 155, 155,
      155, 155, 155, 155, 155, 155, 155, 155, 155, 155,
      155, 155, 155, 155, 155, 155, 155, 155, 155, 155,
      155, 155, 155, 155, 155, 155, 155, 155, 155, 155,
      155, 155, 155, 155, 155, 155, 155, 155, 155, 155,
      155, 155, 155, 155, 155, 155, 155, 155, 155, 155,
      155, 155, 155, 155, 155, 155, 155, 155, 155, 155,
      155, 155, 155, 155, 155, 155, 155, 155, 155, 155,
      155, 155, 155, 155, 155, 155, 155, 155, 155, 155,
      155, 155, 155, 155, 155, 155, 155, 155, 155, 155,
      155, 155, 155, 155, 155, 155, 155, 155, 155, 155,
      155, 155, 155, 155, 155, 155, 155, 155, 155, 155,
      155, 155, 155, 155, 155, 155, 155, 155, 155, 155,
      155, 155, 155, 155, 155, 155
    };
  return len + asso_values[(unsigned char)str[2]] + asso_values[(unsigned char)str[1]] + asso_values[(unsigned char)str[0]];
}

const struct metro_location *
in_word_set_metro (str, len)
     register const char *str;
     register unsigned int len;
{
  static const struct metro_location wordlist[] =
    {
#line 8 "./metro_gperf.txt"
      {"ANC", 61.2043850, -149.8734160, 50},
#line 56 "./metro_gperf.txt"
      {"SLC", 40.7677204, -111.9011271, 50},
#line 17 "./metro_gperf.txt"
      {"CLE", 41.4967040, -81.6894820, 50},
#line 32 "./metro_gperf.txt"
      {"LAS", 36.1011980, -115.1724240, 50},
#line 51 "./metro_gperf.txt"
      {"SAN", 32.7222780, -117.1523000, 50},
#line 18 "./metro_gperf.txt"
      {"CLT", 35.2247280, -80.8431530, 100},
#line 36 "./metro_gperf.txt"
      {"MEM", 35.1394710, -90.0514930, 50},
#line 9 "./metro_gperf.txt"
      {"ATL", 33.7565290, -84.4009960, 50},
#line 54 "./metro_gperf.txt"
      {"SEA", 47.5951840, -122.3331770, 50},
#line 58 "./metro_gperf.txt"
      {"STL", 38.6239780, -90.1940500, 50},
#line 35 "./metro_gperf.txt"
      {"MCO", 28.5398320, -81.3840450, 50},
#line 52 "./metro_gperf.txt"
      {"SAT", 29.4276700, -98.4167680, 50},
#line 42 "./metro_gperf.txt"
      {"OMA", 41.2637186, -95.9292658, 50},
#line 34 "./metro_gperf.txt"
      {"MCI", 39.0971540, -94.5810090, 50},
#line 33 "./metro_gperf.txt"
      {"LAX", 34.0430320, -118.2668460, 50},
#line 37 "./metro_gperf.txt"
      {"MIA", 25.7812320, -80.1878050, 50},
#line 38 "./metro_gperf.txt"
      {"MKE", 43.0304190, -87.9111090, 50},
#line 39 "./metro_gperf.txt"
      {"MSP", 44.9796570, -93.2748890, 50},
#line 12 "./metro_gperf.txt"
      {"BNA", 36.1599490, -86.7785700, 50},
#line 21 "./metro_gperf.txt"
      {"DEN", 39.7472400, -105.0101660, 50},
#line 23 "./metro_gperf.txt"
      {"DSM", 41.5926340, -93.6209820, 50},
#line 41 "./metro_gperf.txt"
      {"OKC", 35.4633910, -97.5145940, 50},
#line 59 "./metro_gperf.txt"
      {"TPA", 27.9777810, -82.5053670, 50},
#line 26 "./metro_gperf.txt"
      {"HNL", 21.3715550, -157.9297430, 50},
#line 19 "./metro_gperf.txt"
      {"CMH", 39.9694000, -83.0063560, 50},
#line 10 "./metro_gperf.txt"
      {"AUS", 30.2835480, -97.7313490, 50},
#line 57 "./metro_gperf.txt"
      {"SMF", 38.6480050, -121.5170770, 50},
#line 40 "./metro_gperf.txt"
      {"MSY", 29.9522110, -90.0805630, 50},
#line 20 "./metro_gperf.txt"
      {"CVG", 39.0976360, -84.5087510, 50},
#line 29 "./metro_gperf.txt"
      {"IND", 39.7602200, -86.1620040, 50},
#line 50 "./metro_gperf.txt"
      {"RNO", 39.5454190, -119.8156930, 50},
#line 27 "./metro_gperf.txt"
      {"IAD", 38.8974120, -77.0200290, 25},
#line 14 "./metro_gperf.txt"
      {"BOS", 42.3662140, -71.0611570, 50},
#line 49 "./metro_gperf.txt"
      {"RIC", 37.5446270, -77.4344960, 50},
#line 60 "./metro_gperf.txt"
      {"TYS", 35.9620650, -83.9110160, 50},
#line 47 "./metro_gperf.txt"
      {"PIT", 40.4404480, -79.9891980, 50},
#line 55 "./metro_gperf.txt"
      {"SFO", 37.7786440, -122.3893800, 50},
#line 45 "./metro_gperf.txt"
      {"PHL", 39.9042715, -75.1713324, 50},
#line 30 "./metro_gperf.txt"
      {"JAX", 30.3235480, -81.6353920, 50},
#line 28 "./metro_gperf.txt"
      {"IAH", 29.7568150, -95.3572940, 50},
#line 7 "./metro_gperf.txt"
      {"ABQ", 35.0664970, -106.6315730, 50},
#line 53 "./metro_gperf.txt"
      {"SDF", 44.8936820, -75.0144570, 50},
#line 13 "./metro_gperf.txt"
      {"BOI", 43.6056600, -116.2059970, 50},
#line 11 "./metro_gperf.txt"
      {"BHM", 33.3313930, -86.7883630, 50},
#line 44 "./metro_gperf.txt"
      {"PDX", 45.5334360, -122.6708540, 50},
#line 24 "./metro_gperf.txt"
      {"DTW", 42.3387530, -83.0484870, 50},
#line 43 "./metro_gperf.txt"
      {"ORD", 41.8812600, -87.6741450, 50},
#line 25 "./metro_gperf.txt"
      {"GRB", 44.5009130, -88.0622860, 50},
#line 46 "./metro_gperf.txt"
      {"PHX", 33.4458990, -112.0713130, 50},
#line 16 "./metro_gperf.txt"
      {"BWI", 39.2813010, -76.6201220, 75},
#line 31 "./metro_gperf.txt"
      {"JFK", 40.7503540, -73.9933710, 50},
#line 48 "./metro_gperf.txt"
      {"RDU", 35.8034750, -78.7219330, 100},
#line 22 "./metro_gperf.txt"
      {"DFW", 32.7898920, -96.8113060, 50},
#line 15 "./metro_gperf.txt"
      {"BUF", 42.8749520, -78.8760460, 100}
    };

  if (len <= MAX_WORD_LENGTH && len >= MIN_WORD_LENGTH)
    {
      register int key = hash_metro (str, len);

      if (key <= MAX_HASH_VALUE && key >= MIN_HASH_VALUE)
        {
          register const struct metro_location *resword;

          switch (key - 9)
            {
              case 0:
                resword = &wordlist[0];
                goto compare;
              case 4:
                resword = &wordlist[1];
                goto compare;
              case 5:
                resword = &wordlist[2];
                goto compare;
              case 9:
                resword = &wordlist[3];
                goto compare;
              case 10:
                resword = &wordlist[4];
                goto compare;
              case 14:
                resword = &wordlist[5];
                goto compare;
              case 15:
                resword = &wordlist[6];
                goto compare;
              case 19:
                resword = &wordlist[7];
                goto compare;
              case 20:
                resword = &wordlist[8];
                goto compare;
              case 24:
                resword = &wordlist[9];
                goto compare;
              case 25:
                resword = &wordlist[10];
                goto compare;
              case 29:
                resword = &wordlist[11];
                goto compare;
              case 30:
                resword = &wordlist[12];
                goto compare;
              case 34:
                resword = &wordlist[13];
                goto compare;
              case 35:
                resword = &wordlist[14];
                goto compare;
              case 39:
                resword = &wordlist[15];
                goto compare;
              case 40:
                resword = &wordlist[16];
                goto compare;
              case 44:
                resword = &wordlist[17];
                goto compare;
              case 45:
                resword = &wordlist[18];
                goto compare;
              case 46:
                resword = &wordlist[19];
                goto compare;
              case 49:
                resword = &wordlist[20];
                goto compare;
              case 50:
                resword = &wordlist[21];
                goto compare;
              case 54:
                resword = &wordlist[22];
                goto compare;
              case 55:
                resword = &wordlist[23];
                goto compare;
              case 59:
                resword = &wordlist[24];
                goto compare;
              case 60:
                resword = &wordlist[25];
                goto compare;
              case 64:
                resword = &wordlist[26];
                goto compare;
              case 65:
                resword = &wordlist[27];
                goto compare;
              case 69:
                resword = &wordlist[28];
                goto compare;
              case 70:
                resword = &wordlist[29];
                goto compare;
              case 71:
                resword = &wordlist[30];
                goto compare;
              case 74:
                resword = &wordlist[31];
                goto compare;
              case 75:
                resword = &wordlist[32];
                goto compare;
              case 79:
                resword = &wordlist[33];
                goto compare;
              case 80:
                resword = &wordlist[34];
                goto compare;
              case 84:
                resword = &wordlist[35];
                goto compare;
              case 85:
                resword = &wordlist[36];
                goto compare;
              case 89:
                resword = &wordlist[37];
                goto compare;
              case 90:
                resword = &wordlist[38];
                goto compare;
              case 94:
                resword = &wordlist[39];
                goto compare;
              case 95:
                resword = &wordlist[40];
                goto compare;
              case 99:
                resword = &wordlist[41];
                goto compare;
              case 100:
                resword = &wordlist[42];
                goto compare;
              case 104:
                resword = &wordlist[43];
                goto compare;
              case 105:
                resword = &wordlist[44];
                goto compare;
              case 109:
                resword = &wordlist[45];
                goto compare;
              case 110:
                resword = &wordlist[46];
                goto compare;
              case 114:
                resword = &wordlist[47];
                goto compare;
              case 125:
                resword = &wordlist[48];
                goto compare;
              case 129:
                resword = &wordlist[49];
                goto compare;
              case 134:
                resword = &wordlist[50];
                goto compare;
              case 135:
                resword = &wordlist[51];
                goto compare;
              case 144:
                resword = &wordlist[52];
                goto compare;
              case 145:
                resword = &wordlist[53];
                goto compare;
            }
          return 0;
        compare:
          {
            register const char *s = resword->iata;

            if (*str == *s && !strcmp (str + 1, s + 1))
              return resword;
          }
        }
    }
  return 0;
}
#line 61 "./metro_gperf.txt"

