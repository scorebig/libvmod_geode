/*VMOD boilerplate*/
#include <stdlib.h>
#include "vrt.h"
#include "bin/varnishd/cache.h"
#include "vcc_if.h"

/* Our headers */
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <GeoIP.h>
#include <GeoIPCity.h>
#include "51Degrees.h"
#include "metro_loc.h"
#include "metros.h"

#define GEOLITE_CITY_LOCATION "/usr/share/GeoIP/GeoLiteCity.dat"
#define MAX_VALUE_LENGTH 300
#define DD_DATA_FILE "/etc/varnish/51Degrees-Lite.dat"
#define DD_REQUIRED_PROPERTIES "IsMobile,ScreenPixelsHeight,ScreenPixelsWidth"
#define DD_ISMOBILE 0
#define DD_SCREENHEIGHT 1
#define DD_SCREENWIDTH 2
#define R 6371
#define TO_RAD (3.1415926536 / 180)

/* Static error messages */
const char *error_db = "(db not initialized!)";
const char *error_ip_unknown = "(IP not found)";
const char *error_bad_ip = "(bad IP string!)";
const char *error_loc_unknown = "(coordinates not found)";
const char *error_long_unknown = "(longitude not found)";
const char *error_lat_unknown = "(latitude not found)";
const char *error_postal_unknown = "(postal code not found)";
const char *error_device_unknown = "(none)";
const char *error_device_value_unknown = "(value unknown)";
const char *error_device_value_error = "(value error)";

typedef struct metro_distance {
    const char *iata;
    float distance;
} MetroDistance;
    
struct databases {
    DataSet *ds; /* 51Degrees DataSet */
    GeoIP *city;
};

int release_51degrees(DataSet *ds) {
    destroy(ds);
    free(ds);
    return (0);
}

int release_geoip(GeoIP *gi) {
    GeoIP_delete(gi);
    return (0);
}

int release_databases(struct databases *db) {
    release_51degrees(db->ds);
    release_geoip(db->city);
    free(db);
    return (0);
}

int init_51degrees(struct vmod_priv *pp) {
    struct databases *db = (struct databases *) pp->priv;
    DataSetInitStatus ret;
   /* initWithPropertyString actually mutates its input required_properties argument,
    so can't be a const */
    char *req_properties = malloc(sizeof(DD_REQUIRED_PROPERTIES));
    db->ds = (DataSet *) malloc(sizeof(DataSet));
    if (!db->ds || !req_properties) {
        return (-1);
    }
    strncpy(req_properties, DD_REQUIRED_PROPERTIES, sizeof(DD_REQUIRED_PROPERTIES));
    ret = initWithPropertyString(DD_DATA_FILE, db->ds, req_properties);
    free(req_properties);
    switch (ret) {
        case DATA_SET_INIT_STATUS_INSUFFICIENT_MEMORY:
            printf("geode: Insufficient memory to load 51Degrees data file\n");
            return (-1);
        case DATA_SET_INIT_STATUS_CORRUPT_DATA:
            printf("geode: 51Degrees device data file is corrupted\n");
            return (-1);
        case DATA_SET_INIT_STATUS_INCORRECT_VERSION:
            printf("geode: 51Degrees device data file is not correct version\n");
            return (-1);
        case DATA_SET_INIT_STATUS_FILE_NOT_FOUND:
            printf("geode: 51Degrees device data file not found\n");
            return (-1);
        default: {
            return (0);
        }
    } 
    return (-1);
}

int init_geoip(struct vmod_priv *pp) {
    struct databases *db = (struct databases *) pp->priv;
    db->city = GeoIP_open(GEOLITE_CITY_LOCATION, GEOIP_MEMORY_CACHE);
    if (!db->city) {
        printf("geode: Error opening GeoIP database\n");
        return (-1);
    }
    return (0);
}

int
init_function(struct vmod_priv *pp, const struct VCL_conf *conf)
{
    /* Preallocate databases */
    pp->priv = (struct databases *) malloc(sizeof(struct databases));
    if (!pp->priv) {
        return (-1);
    }
    pp->free = (vmod_priv_free_f *) &release_databases;
    
    if (init_51degrees(pp) < 0) {
        return (1);
    }
    
    if (init_geoip(pp) < 0) {
        return (1);
    }
	return (0);
}

Workset * domatch(DataSet *ds, const char *user_agent) {
    int i = strlen(user_agent);
    if (i > 0) {
        Workset *ws = NULL;
        ws = createWorkset(ds);
        strncpy(ws->input, user_agent, i);
        match(ws, ws->input);
        return ws;
    }
    return (NULL);
}

const char * copy_workspace(struct sess *sp, const char *value, unsigned int value_size) {
    /* Copy string to workspace such that it can be used in subsequent VCL */
    unsigned reserved_size;
    char *front;

    reserved_size = WS_Reserve(sp->wrk->ws, 0);
    if (value_size > reserved_size) {
        /* not enough workspace room */
        WS_Release(sp->wrk->ws, 0);
        return (NULL);
    }
    front = sp->wrk->ws->f; /* front of reserved space */
    strncpy(front, value, value_size+1);
    WS_Release(sp->wrk->ws, value_size);
    return (front);
}

const char * dd_getvalue(struct sess *sp, struct vmod_priv *pp, const char *user_agent, int value_type) {
    /* Get device detection value */
    const char *value;
    unsigned value_size;
    struct databases *db = (struct databases *) pp->priv;
    Workset *ws = domatch(db->ds, user_agent);
    
    if (!ws) {
        return (copy_workspace(sp, error_device_unknown, strnlen(error_device_unknown, MAX_VALUE_LENGTH)));
    }
    if (setValues(ws, value_type) != 1) {
        return (copy_workspace(sp, error_device_value_unknown, strnlen(error_device_value_unknown, MAX_VALUE_LENGTH)));
    }
    value = getValueName(db->ds, *(ws->values));
    freeWorkset(ws);
    if (!value) {
        return (copy_workspace(sp, error_device_value_error, strnlen(error_device_value_error, MAX_VALUE_LENGTH)));
    }
    value_size = strnlen(value, MAX_VALUE_LENGTH);
    return (copy_workspace(sp, value, value_size));
}

/* Haversine Formula - returns distance between points in km */
double dist(double lat1, double long1, double lat2, double long2)
{
	double dx, dy, dz;
	long1 -= long2;
	long1 *= TO_RAD, lat1 *= TO_RAD, lat2 *= TO_RAD;
 
	dz = sin(lat1) - sin(lat2);
	dx = cos(long1) * cos(lat1) - cos(lat2);
	dy = sin(long1) * cos(lat1);
	return asin(sqrt(dx * dx + dy * dy + dz * dz) / 2) * 2 * R;
}

MetroDistance find_closest_metro(float latitude, float longitude) {
    unsigned int m_count = sizeof(metros) / sizeof(metros[0]);
    unsigned int i;
    const struct metro_location *m_struct;
    MetroDistance mdist[m_count];
    MetroDistance *closest = NULL;
    
    for (i = 0; i < m_count; i++) {
        m_struct = in_word_set_metro(metros[i], 3);
        if (!m_struct) {
            printf("ERROR: metro not found in hash table!\n");
            mdist[0].iata = NULL;
            return mdist[0];
        }
        mdist[i].distance = (float) dist((double) latitude, (double) longitude, (double) m_struct->latitude, (double) m_struct->longitude) / 1.609344; /* km -> miles */
        mdist[i].iata = metros[i];    
    }
    
    for (i = 0; i < m_count; i++) {
        if (!closest) {
            closest = &mdist[i];
        } else if (mdist[i].distance < closest->distance) {
            closest = &mdist[i];
        }
    }
    //printf("computed closest metro: %s\n", closest->iata);
    //printf("computed closest distance: %f\n", closest->distance);
    return (*closest);
}

const char * vmod_ismobile(struct sess *sp, struct vmod_priv *pp, const char *user_agent) {
    if (user_agent) {
        return (dd_getvalue(sp, pp, user_agent, DD_ISMOBILE));
    } else {
        return (copy_workspace(sp, error_device_unknown, strnlen(error_device_unknown, MAX_VALUE_LENGTH)));
    }
}

const char * vmod_mobile_screenheight(struct sess *sp, struct vmod_priv *pp, const char *user_agent) {
    if (user_agent) {
        return (dd_getvalue(sp, pp, user_agent, DD_SCREENHEIGHT));
    } else {
        return (copy_workspace(sp, error_device_unknown, strnlen(error_device_unknown, MAX_VALUE_LENGTH)));
    }
}
    
const char * vmod_mobile_screenwidth(struct sess *sp, struct vmod_priv *pp, const char *user_agent) {
    if (user_agent) {
        return (dd_getvalue(sp, pp, user_agent, DD_SCREENWIDTH));
    } else {
        return (copy_workspace(sp, error_device_unknown, strnlen(error_device_unknown, MAX_VALUE_LENGTH)));
    }
}   

const char * vmod_zipcode(struct sess *sp, struct vmod_priv *pp, const char *ip) {
    GeoIPRecord *gir;
    struct databases *db = (struct databases *) pp->priv;
    char *postal_code;
    if (!db->city) {
        return (copy_workspace(sp, error_db, strnlen(error_db, MAX_VALUE_LENGTH)));
    }
    if (!ip) {
        return (copy_workspace(sp, error_bad_ip, strnlen(error_bad_ip, MAX_VALUE_LENGTH)));
    }
    gir = GeoIP_record_by_addr(db->city, ip);
    if (!gir) {
        return (copy_workspace(sp, error_ip_unknown, strnlen(error_ip_unknown, MAX_VALUE_LENGTH)));
    }
    if (!gir->postal_code) {
        return (copy_workspace(sp, error_postal_unknown, strnlen(error_postal_unknown, MAX_VALUE_LENGTH)));
    }
    postal_code = gir->postal_code;
    GeoIPRecord_delete(gir);
    return (copy_workspace(sp, gir->postal_code, strnlen(gir->postal_code, MAX_VALUE_LENGTH)));
}

const char * vmod_latitude(struct sess *sp, struct vmod_priv *pp, const char *ip) {
    char buffer[MAX_VALUE_LENGTH];  /* buffer for stringified float */
    unsigned value_length;
    GeoIPRecord *gir;
    struct databases *db = (struct databases *) pp->priv;
    if (!db->city) {
        return (copy_workspace(sp, error_db, strnlen(error_db, MAX_VALUE_LENGTH)));
    }
    if (!ip) {
        return (copy_workspace(sp, error_bad_ip, strnlen(error_bad_ip, MAX_VALUE_LENGTH)));
    }
    gir = GeoIP_record_by_addr(db->city, ip);
    if (!gir) {
        return (copy_workspace(sp, error_ip_unknown, strnlen(error_ip_unknown, MAX_VALUE_LENGTH)));
    }
    if (!gir->latitude) {
        return (copy_workspace(sp, error_lat_unknown, strnlen(error_lat_unknown, MAX_VALUE_LENGTH)));
    }
    value_length = snprintf((char *) &buffer, MAX_VALUE_LENGTH, "%f", gir->latitude);
    GeoIPRecord_delete(gir);
    return (copy_workspace(sp, (const char *) &buffer, value_length));
}

const char * vmod_longitude(struct sess *sp, struct vmod_priv *pp, const char *ip) {
    char buffer[MAX_VALUE_LENGTH];  /* buffer for stringified float */
    unsigned value_length;
    GeoIPRecord *gir;
    struct databases *db = (struct databases *) pp->priv;
    if (!db->city) {
        return (copy_workspace(sp, error_db, strnlen(error_db, MAX_VALUE_LENGTH)));
    }
    if (!ip) {
        return (copy_workspace(sp, error_bad_ip, strnlen(error_bad_ip, MAX_VALUE_LENGTH)));
    }
    gir = GeoIP_record_by_addr(db->city, ip);
    if (!gir) {
        return (copy_workspace(sp, error_ip_unknown, strnlen(error_ip_unknown, MAX_VALUE_LENGTH)));
    }
    if (!gir->longitude) {
        return (copy_workspace(sp, error_long_unknown, strnlen(error_long_unknown, MAX_VALUE_LENGTH)));
    }
    value_length = snprintf((char *) &buffer, MAX_VALUE_LENGTH, "%f", gir->longitude);
    GeoIPRecord_delete(gir);
    return (copy_workspace(sp, (const char *) &buffer, value_length));
}

/* measured overhead: ~70 - 100 usec per request */
const char * vmod_metrocode(struct sess *sp, struct vmod_priv *pp, const char *ip) {
    MetroDistance closest_metro;
    GeoIPRecord *gir;
    struct databases *db = (struct databases *) pp->priv;
    if (!db->city) {
        return (copy_workspace(sp, error_db, sizeof(error_db)));
    }
    if (!ip) {
        return (copy_workspace(sp, error_bad_ip, strnlen(error_bad_ip, MAX_VALUE_LENGTH)));
    }
    gir = GeoIP_record_by_addr(db->city, ip);
    if (!gir) {
        return (copy_workspace(sp, error_ip_unknown, strnlen(error_ip_unknown, MAX_VALUE_LENGTH)));
    }
    if (!gir->longitude || !gir->latitude) {
        return (copy_workspace(sp, error_loc_unknown, strnlen(error_loc_unknown, MAX_VALUE_LENGTH)));
    }

    closest_metro = find_closest_metro(gir->latitude, gir->longitude);
    GeoIPRecord_delete(gir);
    if (!closest_metro.iata) {
        return ("(error)");
    } 
    return (copy_workspace(sp, closest_metro.iata, 3));
}
