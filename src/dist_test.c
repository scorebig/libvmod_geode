#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define R 6371
#define TO_RAD (3.1415926536 / 180)

/* Haversine Formula */
double dist(double lat1, double long1, double lat2, double long2)
{
	double dx, dy, dz;
	long1 -= long2;
	long1 *= TO_RAD, lat1 *= TO_RAD, lat2 *= TO_RAD;
 
	dz = sin(lat1) - sin(lat2);
	dx = cos(long1) * cos(lat1) - cos(lat2);
	dy = sin(long1) * cos(lat1);
	return asin(sqrt(dx * dx + dy * dy + dz * dz) / 2) * 2 * R;
}
 
int main()
{
    /*JFK: Latitude	Longitude 40.7503540	-73.9933710
      LAX: Latitude	Longitude 34.0430320	-118.2668460 
    */
	double d = dist(40.75, -73.99, 34.04, -118.27);
	/* Americans don't know kilometers */
	printf("dist: %.1f km (%.1f mi.)\n", d, d / 1.609344);
 
	return 0;
}